from fabric.api import cd, run
from drupal import Drupal


class Pantheon(Drupal):
    '''
    Dm class for a Pantheon hosted site development platform.
    '''

    def drush_db_pull(self, dst_alias, src_alias, db_key='default',
                      options=''):
        run('{drush} @{alias} cc drush'.format(drush=self.drush(),
            alias=src_alias))
        cmd_sync = '{drush} sql-sync-pipe -y ' + \
            ' '.join(options['sql-sync-pipe']) + ' ' + \
            '--tables-key={db_key} ' + \
            '@{src} @{dst}'
        run(cmd_sync.format(drush=self.drush(), db_key=db_key, src=src_alias,
            dst=dst_alias))

    def filespull(self):
        for site, c in self.d().drupal.sites.iteritems():
            # public
            dst = self.drupal_assets_public_dir(site)
            self.system().chown(
                dst, user=self.system().whoami(),
                group=self.system().group('server'), options='-R')
            src = self.drupal_site_source_conf(site).drush.alias.name
            options = ' '.join(map(
                lambda e: '--exclude={0}'.format(e),
                c.files.public.exclude))
            cmd_sync = '{drush} -y rsync --exclude=/private/ --verbose ' + \
                '{opts} @{src}:%files/ .'
            with cd(dst):
                run(cmd_sync.format(drush=self.drush(), opts=options, src=src))
            # private
            dst = self.drupal_assets_private_dir(site)
            self.system().chown(
                dst, user=self.system().whoami(),
                group=self.system().group('server'), options='-R')
            src = self.drupal_site_source_conf(site).drush.alias.name
            options = ' '.join(map(
                lambda e: '--exclude={0}'.format(e),
                c.files.public.exclude))
            cmd_sync = '{drush} -y rsync --verbose ' + \
                '{opts} @{src}:%files/private/ .'
            with cd(dst):
                run(cmd_sync.format(drush=self.drush(), opts=options, src=src))
