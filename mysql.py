from fabric.api import hide, run, settings
from . import Database
import pipes


class MySQL(Database):
    '''
    Dm class for a MySql installation.
    '''

    def databases(self, conn):
        with hide('stdout'):
            return self.execute(conn, 'SHOW DATABASES;').split()[1:]

    def execute(self, conn, sql=None, path=None, database=None):
        cmd = "mysql "
        if conn.username:
            cmd += "-u '{user}' ".format(user=conn.username)
        if conn.password:
            cmd += "-p '{pwd}' ".format(pwd=conn.password)
        if conn.host:
            cmd += "-h '{host}' ".format(host=conn.host)
        if conn.port:
            cmd += "-P {port} ".format(port=conn.port)
        if database is not None:
            cmd += "{db} ".format(db=database)
        if sql is not None:
            cmd = 'echo {sql} | {cmd}'.format(cmd=cmd, sql=pipes.quote(sql))
        elif path is not None:
            if path.split('.')[-1] == 'gz':
                cmd = "pv {path} | gunzip -c | {cmd}".format(
                    cmd=cmd, path=path)
            else:
                cmd = "pv {path} | {cmd}".format(cmd=cmd, path=path)
        with settings(warn_only=True):
            return run(cmd)
