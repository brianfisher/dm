from fabric.api import abort, env, put, run, settings
from . import ROLE_SRC, ROLE_DST
from ubuntu import Ubuntu
from drupal import CodeMake, DrushAliases, Drupal, DrupalTask
import os
import re
import StringIO


class DrupalAegirPlatformBuild(CodeMake, DrushAliases):
    '''Builds an Aegir platform.'''
    name = 'platform-build'
    dependencies = []

    def run(self):
        '''
        Builds an Aegir platform -- creates platform and site aliases, builds
        the codebase using drush make.
        '''
        env.dm.aegirplatformbuild()

    def configure(self):
        # we use drush make to build the platform
        CodeMake.configure(self)
        # need drush aliases
        DrushAliases.configure(self)

        cnf = env.dm.conf()

        path = ['drupal', 'drush', 'alias', 'name']
        cnf.dst_validate(path, self.validate_platform_alias_name)

        # each site
        for site in env.dm.d().drupal.sites.iterkeys():
            path = ['drupal', 'sites', site]
            # url
            cnf.dst_require([path + ['url']])
            # vars
            self.configure_vars()

    def configure_alias(self, conf, platform, uri, root, host, role):
        if role == ROLE_SRC:
            if not conf.name:
                abort('Source platform and site need drush/alias/name.')
        if role == ROLE_DST:
            # aegir alias conventions
            if not uri:
                conf.name = 'platform_{p}'.format(p=platform)
            else:
                conf.name = uri
            conf.value = self.configure_alias_value(root, host, uri)

    def validate_platform_alias_name(self, value, args):
        # aegir platform alias convention
        if re.match(r'[^a-zA-Z0-0_]', value):
            msg = 'Platform key "{0}" must contain only letters, numbers, and '
            msg += 'underscores.'
            return msg.format(value)

aegirplatformbuild = DrupalAegirPlatformBuild()


class DrupalAegirPlatformClean(DrupalTask):
    '''Deletes an Aegir platform.'''
    name = 'platform-clean'

    def run(self):
        env.dm.aegirplatformclean()

aegirplatformclean = DrupalAegirPlatformClean()


class DrupalAegirSiteBuild(DrupalTask):
    '''Builds an Aegir site on an Aegir platform.'''
    name = 'site-build'

    def run(self, site=None):
        '''
        Installs an Aegir site using a profile on an Aegir platform.

        Keyword arguments:
        site -- (optional) The site to build, if missing will build all.
        '''
        env.dm.aegirsitebuild(site)

    def configure(self):
        DrupalTask.configure(self)
        cnf = env.dm.conf()

        for site in env.dm.d().drupal.sites.iterkeys():

            path = ['drupal', 'sites', site, 'profile']
            if site == env.dm.drupal_site_source_key(site):
                cnf.inherit(path)
            cnf.dst_require([path])

aegirsitebuild = DrupalAegirSiteBuild()


class DrupalAegirSiteClean(DrupalTask):
    '''Deletes an Aegir site from an Aegir platform.'''
    name = 'site-clean'

    def run(self):
        env.dm.aegirsiteclean()

aegirsiteclean = DrupalAegirSiteClean()


class Aegir(Ubuntu):

    sudo = False


class DrupalAegir(Drupal):
    '''
    Dm class for an Aegir hosted site development platform.

    @see http://brianfisher.name/node/47
    '''

    def configure(self, dest, source=None):
        Drupal.configure(self, dest, source)
        self.d().path.base = '/data/disk/o1'
        self.d().path.code = env.dm.role_key()

    # tasks

    def aegirplatformbuild(self):
        alias = self.d().drupal.drush.alias.name

        # checkout make file
        conf_make = self.d().drupal.drush.make
        self.drush_makefile_checkout(conf_make)

        # create alias
        cmd = "{drush} --root='{root}' --make_working_copy='TRUE'"
        cmd += " provision-save '@{alias}' --context_type='platform'"
        cmd += " --makefile='{d}/{f}'"
        cmd = cmd.format(
            drush=self.drush(), root=self.code_dir(),
            alias=alias,
            d=self.drush_makefile_dir(), f=conf_make.file)
        run(cmd)

        # build
        cmd = "drush @{alias} provision-verify".format(alias=alias)
        run(cmd)
        # import into aegir frontend
        cmd = "drush @hostmaster hosting-import @{alias}".format(alias=alias)
        run(cmd)
        cmd = "drush @hostmaster hosting-tasks"
        run(cmd)

        msg = "created platform {alias}"
        print(msg.format(alias=alias))

    def aegirplatformclean(self):
        alias = self.d().drupal.drush.alias.name
        cmd = "drush @hostmaster hosting-task --force @{alias} delete".format(
            alias=alias)
        run(cmd)

    def aegirsitebuild(self, site=None):
        if site:
            sites = [site]
        else:
            sites = self.d().drupal.sites
        for site in sites:
            c = self.d().drupal.sites[site]

            # create alias
            cmd = "drush provision-save '@{s}' --context_type='site' "
            cmd += "--uri='{u}' --platform='@{p}' --server='@server_master' "
            cmd += "--db_server='@server_localhost' --profile='{profile}' "
            cmd += "--client_name='admin'"
            cmd = cmd.format(
                s=c.drush.alias.name,
                u=c.drush.alias.value.uri,
                profile=c.profile,
                p=self.d().drupal.drush.alias.name)
            run(cmd)

            # install site
            cmd = "drush @{s} provision-install".format(
                s=c.drush.alias.name)
            run(cmd)

            # import into aegir frontend
            # if running shortly after build, verify may already be queued
            with settings(warn_only=True):
                cmd = "drush @hostmaster hosting-task @{p} verify --force"
                cmd = cmd.format(p=self.d().drupal.drush.alias.name)
                run(cmd)

            if c.settings.php:
                php = self.php_header()
                php += "\n" + c.settings.php + "\n"
                path = '{b}/sites/{s}/local.settings.php'.format(
                    b=self.code_dir(), s=site)
                self.system().chmod(paths=path, mode='666')
                put(StringIO.StringIO(php), path)
                self.system().chmod(paths=path, mode='440')
                self.system().chgrp(
                    paths=os.path.dirname(path),
                    group=self.system().group('server'))
                self.system().chmod(paths=os.path.dirname(path), mode='550')

        for site in sites:

            # generate login link
            self.drush_run('uli', site=site)

    def aegirsiteclean(self, site=None):
        if site:
            sites = [site]
        else:
            sites = self.d().drupal.sites
        for site in sites:
            alias = self.d().drupal.sites[site].drush.alias.name
            cmd = "drush @hostmaster hosting-task --force @{s} delete"
            cmd = cmd.format(s=alias)
            run(cmd)

    # utilities

    def aegir_platform(self):
        '''Get the current platform alias.'''
        raise NotImplementedError

    def aegir_platforms_existing(self):
        '''List currently installed platforms.'''
        if self.aegir_platforms is None:
            self.aegir_platforms = {}
            aliases = run("{drush} sa".format(self.drush())).split()
            for alias in aliases:
                if alias[:10] == '@platform_':
                    self.aegir_platforms[alias[1:]] = None
        return self.aegir_platforms

    def drush_makefile_dir(self, role=None):
        '''The platform assets directory'''
        if role is not None:
            Drupal.drush_makefile_dir(self, role)
        return '/'.join([
            self.assets_dir(site=self.conf().dst_key()),
            self.DRUPAL_ASSETS_MAKEFILE_DIR])
