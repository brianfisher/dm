from fabric.api import (
    abort, env, execute, hide, put, run, settings, sudo, warn)
from fabric.contrib import files
from . import Dm, DmTask, VirtualHost, ROLE_SRC, ROLE_DST
from lya import AttrDict
import os
import json
import StringIO
import urllib
import pkg_resources
from textwrap import dedent
import pipes
import re

# fabric commands


class DrupalTask(DmTask):
    '''Configuration routines shared by tasks.'''

    def configure_db(self):
        cnf = env.dm.conf()
        for d in env.dm.d().drupal.sites.itervalues():
            default = env.dm.yaml_pkg_load('conf/drupal-site-db.yaml')
            for db in d.db.itervalues():
                (cnf.rebase(db, [], default)
                    .require(db, [['connection', 'database']]))

    def configure_vars(self):
        cnf = env.dm.conf()
        for site, d in env.dm.d().drupal.sites.iteritems():
            default = env.dm.yaml_pkg_load('conf/drupal-site-var.yaml')
            for var in d.vars.iterkeys():
                path = ['drupal', 'sites', site, 'vars', var]
                cnf.dst_rebase(path, default)

    def require_aliases(self):
        '''Checks that DrushAliases has been run or aliases already exist.'''
        for site, d in env.dm.d().drupal.sites.iteritems():
            msg = 'task {t} requires drush alias for {r}.'
            if 'name' not in d.drush.alias:
                abort(msg.format(t=self.name, r='destination'))
            s = env.dm.drupal_site_source_conf(site)
            if s and 'name' not in s.drush.alias:
                abort(msg.format(t=self.name, r='source'))


class CodeMake(DrupalTask):
    '''Build the codebase using drush make.'''
    name = 'code-make'

    def run(self):
        env.dm.make()

    def configure(self):
        DrupalTask.configure(self)
        cnf = env.dm.conf()

        path = ['path', 'assets']
        (cnf.dst_require([path])
            .dst_validate(path, self.validate_path, env.dm.assets_dir()))
        path = ['path', 'code']
        (cnf.dst_require([path])
            .dst_validate(path, self.validate_path, env.dm.code_dir()))

        # drush make
        path = ['drupal', 'drush', 'make']
        default = env.dm.yaml_pkg_load('conf/drupal-drush-make.yaml')
        (cnf.inherit(path)
            .dst_require([path])
            .dst_rebase(path, default))

        # scm
        scm_type = env.dm.d().drupal.drush.make.type
        if scm_type == 'git':
            (cnf.dst_require([path + ['file']])
                .dst_require([path + ['url']]))
        else:
            abort('unexpected scm type "{t}"'.format(t=scm_type))

codemake = CodeMake()


class CodeMakeClean(DrupalTask):
    '''Remove drush makefiles.'''
    name = 'code-make-clean'

    def run(self):
        env.dm.makeclean()

codemakeclean = CodeMakeClean()


class DrupalSites(DrupalTask):
    '''Configure sites in a drupal codebase.'''
    name = 'drupal-sites'

    def run(self):
        '''
        Write the drupal settings.php file, set up sites directory, create
        databases(s) if they do not exist.
        '''
        env.dm.drupalsites()

    def configure(self):
        DrupalTask.configure(self)
        cnf = env.dm.conf()

        cnf.inherit(['drupal', 'version'])

        path = ['path', 'code']
        (cnf.dst_require([path])
            .dst_validate(path, self.validate_path, env.dm.code_dir()))

        # each site
        for site, d in env.dm.d().drupal.sites.iteritems():

            path = ['path', 'assets']
            filepath = env.dm.assets_dir(site=site)
            (cnf.dst_require([path])
                .dst_validate(path, self.validate_path, filepath))

            # url
            cnf.require(d, [['url']])

            # vars
            self.configure_vars()

            # dbs
            self.configure_db()

drupalsites = DrupalSites()


class DrushAliases(DrupalTask):
    '''Write drush aliases.'''
    name = 'drush-aliases'
    dependencies = [DrupalSites]

    def run(self):
        '''
        Write drush aliases for the currently configured sites to the local
        user's ~/.drush directory.'''
        env.dm.drushaliases()

    def configure(self):
        DrupalTask.configure(self)
        cnf = env.dm.conf()

        default = env.dm.yaml_pkg_load('conf/drupal-drush-alias.yaml')
        path = ['drupal', 'drush', 'alias']
        (cnf.dst_rebase(path, default)
            .src_rebase(path, default))
        for site in env.dm.d().drupal.sites.iterkeys():
            path = ['drupal', 'sites', site, 'drush', 'alias']
            cnf.dst_rebase(path, default)
        if env.dm.s():
            for site in env.dm.s().drupal.sites.iterkeys():
                path = ['drupal', 'sites', site, 'drush', 'alias']
                cnf.src_rebase(path, default)

        # configure dest aliases
        role = ROLE_DST
        platform = env.dm.role_key(role)
        root = env.dm.code_dir(role)
        # destination drush commands are always local
        host = 'localhost'
        # platform
        alias = env.dm.d().drupal.drush.alias
        self.configure_alias(alias, platform, None, root, host, role)
        # sites
        for site in env.dm.d().drupal.sites.iterkeys():
            alias = env.dm.d().drupal.sites[site].drush.alias
            self.configure_alias(alias, platform, site, root, host, role)

        # configure source aliases
        if env.dm.s():
            role = ROLE_SRC
            platform = env.dm.role_key(role)
            root = env.dm.code_dir(role)
            host = env.dm.s().host
            # platform
            alias = env.dm.s().drupal.drush.alias
            self.configure_alias(alias, platform, None, root, host, role)
            # sites
            for site in env.dm.s().drupal.sites.iterkeys():
                alias = env.dm.s().drupal.sites[site].drush.alias
                self.configure_alias(alias, platform, site, root, host, role)

        self.configure_aliases_unique()

    def configure_alias(self, conf, platform, uri, root, host, role):
        # externally configured
        if conf.name:
            return
        conf.name = self.configure_alias_name(platform, uri, ROLE_DST)
        # value is assumed fully configured
        if not conf.value:
            conf.value = self.configure_alias_value(root, host, uri)

    def configure_alias_name(self, platform, uri, role):
        # generate an alias name
        name = 'dm.{p}'.format(p=platform)
        if uri is not None:
            name += '.{s}'.format(s=uri)
        return name

    def configure_alias_value(self, root, host, uri, forward=False):
        # generate the alias data structure
        value = AttrDict()
        # configure alias
        value.root = root
        if uri:
            value.uri = uri
        if host != 'localhost':
            host = env.dm.parse_host(host)
            value['remote-host'] = host['hostname']
            value['remote-user'] = host['user']
            if forward:
                value['ssh-options'] = '-A'
            port = host['port']
            if port:
                value['ssh-options'] += " -p " + port
        return value

    def configure_aliases_unique(self):
        def is_unique(names, name, msg):
            if name in names:
                abort('Duplicate alias names for:\n  {0}\n  {1}'.format(
                    names[name], msg))
            names[name] = msg
        names = {}
        name = env.dm.d().drupal.drush.alias.name
        msg = 'Destination drupal->drush->alias->name'
        is_unique(names, name, msg)
        for site, c in env.dm.d().drupal.sites.iteritems():
            name = c.drush.alias.name
            msg = 'Destination drupal->sites->{0}->drush->alias->name'
            msg = msg.format(site)
            is_unique(names, name, msg)
        if env.dm.s():
            name = env.dm.s().drupal.drush.alias.name
            msg = 'Source drupal->drush->alias->name'
            is_unique(names, name, msg)
            for site, c in env.dm.s().drupal.sites.iteritems():
                name = c.drush.alias.name
                msg = 'Source drupal->sites->{0}->drush->alias->name'
                msg = msg.format(site)
                is_unique(names, name, msg)

drushaliases = DrushAliases()


class DrushAliasesClean(DrupalTask):
    '''Remove drush aliases.'''
    name = 'drush-aliases-clean'

    def run(self):
        env.dm.drushaliasesclean()

drushaliasesclean = DrushAliasesClean()


class DrupalVirtualHost(VirtualHost):
    '''Configure a virtualhost for the destination site.'''

    def configure_values(self, key):
        if key == 'type':
            return VirtualHost.configure_values(self, key).append(['nginx'])
        return VirtualHost.configure_values(self, type)

virtualhost = DrupalVirtualHost()


class FilesPull(DrupalTask):
    '''Copy files from source to destination.'''
    name = 'files-pull'
    dependencies = [DrushAliases]

    def run(self):
        env.dm.filespull()

    def configure(self):
        DrupalTask.configure(self)
        cnf = env.dm.conf()

        for site in env.dm.d().drupal.sites.iterkeys():

            path = ['path', 'assets']
            filepath = env.dm.assets_dir(site=site)
            (cnf.dst_require([path])
                .dst_validate(path, self.validate_path, filepath))

            # require drush aliases so we can get file paths
            self.require_aliases()

            # files
            path = ['drupal', 'sites', site, 'files']
            default = env.dm.yaml_pkg_load('conf/drupal-site.yaml').files
            for ftype in ['public', 'private']:
                if site == env.dm.drupal_site_source_key(site):
                    cnf.inherit(path + [ftype])
                cnf.dst_rebase(path + [ftype], default[ftype])

filespull = FilesPull()


class FilesClean(DrupalTask):
    '''Remove files from destination.'''
    name = 'files-clean'

    def run(self):
        '''
        Write drush aliases for the currently configured sites to the current
        user's ~/.drush directory.'''
        env.dm.filesclean()

filesclean = FilesClean()


class DbCreate(DrupalTask):
    '''Create empty databases.'''
    name = 'db-create'

    def run(self):
        env.dm.dbcreate()

dbcreate = DbCreate()


class DbPull(DrupalTask):
    '''Copy database(s) from source to destination.'''
    name = 'db-pull'
    # for now, just a wrapper around drush sql-sync
    dependencies = [DrushAliases]

    def run(self):
        env.dm.dbpull()

    def configure(self):
        DrupalTask.configure(self)
        cnf = env.dm.conf()

        for site, d in env.dm.d().drupal.sites.iteritems():

            # for now, just a wrapper around drush sql-sync
            self.require_aliases()

            default = env.dm.yaml_pkg_load('conf/drupal-site.yaml')

            # modules enable/disable (dict)
            path = ['drupal', 'sites', site, 'modules']
            if site == env.dm.drupal_site_source_key(site):
                cnf.inherit(path)
            cnf.dst_rebase(path, default.modules)

            # commands (list)
            path = ['drupal', 'sites', site, 'drush', 'commands']
            if site == env.dm.drupal_site_source_key(site):
                cnf.inherit(path)

            # vars
            self.configure_vars()

            # dbs
            self.configure_db()
            for db in d.db.iterkeys():
                path = ['drupal', 'sites', site, 'db', db]
                if site == env.dm.drupal_site_source_key(site):
                    (cnf.inherit(path + ['sqldump'])
                        .inherit(path + ['drush']))
                cnf.dst_require([
                    path + ['sqldump'],
                    path + ['drush', 'sql-sync'],
                    path + ['drush', 'sql-sync-pipe'],
                    ])


dbpull = DbPull()


class DbClean(DrupalTask):
    '''Drop database(s) associated with the destination site.'''
    name = 'db-clean'

    def run(self):
        env.dm.dbclean()

dbclean = DbClean()


class Pull(DrupalTask):
    '''Copies files and database from source to destination.'''
    name = 'pull'
    role = None

    def run(self):
        '''Alias for "files-pull db-pull".'''
        execute(filespull)
        execute(dbpull)

pull = Pull()

# Drupal site class


class Drupal(Dm):
    '''
    Dm class for a Drupal development platform.
    '''

    # paths
    DRUPAL_ASSETS_MAKEFILE_DIR = 'drush-make'
    DRUPAL_ASSETS_PUBLIC_DIR = 'files/public'
    DRUPAL_ASSETS_PRIVATE_DIR = 'files/private'
    DRUPAL_ASSETS_CONFIG_DIR = 'config'

    _drupal_version = None
    _drush_version = None
    _drush_status = {}
    _drush_vars = {}

    def __init__(self, path, tasks):
        Dm.__init__(self, path, tasks)
        self._drush_status = {ROLE_SRC: {}, ROLE_DST: {}}

    def configure(self, dest, source=None):
        Dm.configure(self, dest, source)

        default = self.yaml_pkg_load('conf/drupal.yaml')
        path = ['drupal']
        self.conf().dst_rebase(path, default)
        if source:
            self.conf().src_rebase(path, default)

        default = self.yaml_pkg_load('conf/drupal-site.yaml')
        path = ['drupal', 'sites']
        for site, d in self.d().drupal.sites.iteritems():
            self.conf().dst_rebase(path + [site], default)
            if source:
                d.source = self.drupal_site_source_key(site)
                self.conf().src_rebase(path + [d.source], default)

    # tasks

    def code_pull(self):
        src = self.code_dir(ROLE_SRC) + '/'
        dst = self.code_dir()
        self.system().mkdir(dst)
        options = self.RSYNC_OPT_DEFAULT
        # this could fail if using non-standard public files directory
        # @todo in config, run ls on host
        options.append('--exclude=/sites/*/files/')
        self.rsync(src, dst, options)

    def dbclean(self):
        for c in self.d().drupal.sites.itervalues():
            for db_c in c.db.itervalues():
                # does this mean we should not delete?
                # note we're calling this in dbpull()
                if not db_c.pull:
                    continue
                conn = db_c.connection
                if conn.database in self.db().databases(conn):
                    sql = "DROP DATABASE {db};".format(db=conn.database)
                    self.db().execute(conn, sql)

    def dbcreate(self):
        for c in self.d().drupal.sites.itervalues():
            for db in c.db.itervalues():
                conn = db.connection
                if conn.database not in self.db().databases(conn):
                    sql = "CREATE DATABASE {db};".format(
                        db=conn.database)
                    self.db().execute(conn, sql)

    def dbpull(self):
        self.dbclean()

        # each site
        for dst_site, dst_c in self.d().drupal.sites.iteritems():

            # database
            for db_k, db_c in dst_c.db.iteritems():
                if not db_c.pull:
                    continue
                c = db_c.connection
                self.db().execute(
                    c, "CREATE DATABASE {db};".format(db=c.database))
                if db_c.sqldump:
                    self.db().execute(
                        c, database=c.database, path=db_c.sqldump)
                else:
                    # drush sql-sync
                    if db_c.pull:
                        src_c = self.drupal_site_source_conf(dst_site)
                        self.drush_db_pull(
                            dst_c.drush.alias.name, src_c.drush.alias.name,
                            db_key=db_k, options=db_c.drush)

            # variables
            for name, var in dst_c.vars.iteritems():
                cmd = 'vset --exact --yes '
                if var.format:
                    cmd += '--format={format} '.format(format=var.format)
                cmd += "{name} {value}".format(
                    name=name,
                    value=pipes.quote(var.value))
                self.drush_run(cmd, site=dst_site)

            # modules
            if len(dst_c.modules.enable) or len(dst_c.modules.disable):
                with hide('stdout'):
                    enabled = self.drush_run(
                        'pm-list --pipe --status=enabled',
                        site=dst_site).split()
                    downloaded = self.drush_run(
                        'pm-list --pipe', site=dst_site).split()
                for module in dst_c.modules.disable:
                    if module in enabled:
                        cmd = 'pm-disable {0}'.format(module)
                        self.drush_run(cmd, site=dst_site)
                    else:
                        warn('module {0} already disabled.'.format(module))
                for module in dst_c.modules.enable:
                    module_name = module.split('-')[0]
                    if module_name not in downloaded:
                        cmd = 'pm-download {0}'.format(module)
                        self.drush_run(cmd, site=dst_site)
                    if module_name not in enabled:
                        cmd = 'pm-enable {0}'.format(module_name)
                        self.drush_run(cmd, site=dst_site)
                    else:
                        warn('module {0} already enabled.'.format(module_name))
                self.drush_run('cc all', site=dst_site)

            # commands
            for cmd in dst_c.drush.commands:
                self.drush_run(cmd, site=dst_site)

        # generate login link
        for dst_site, dst_c in self.d().drupal.sites.iteritems():
            self.drush_run('uli', site=dst_site)

    def make(self):
        c = self.d().drupal.drush.make
        code_dir = self.code_dir()
        mf_dir = self.drush_makefile_dir()

        if files.exists(code_dir):
            abort('code directory "{p}" already exists.'.format(p=code_dir))
        if not files.exists(self.base_dir()):
            self.system().mkdir(self.base_dir())

        self.drush_makefile_checkout(c)

        # run
        # fails if cd exists
        cmd = "{drush} make --yes {opt} {d}/{f} {cd}".format(
            drush=self.drush(), opt=c.options, d=mf_dir, f=c.file, cd=code_dir)
        run(cmd, True)

        self.system().chgrp(
            paths=code_dir, group=self.system().group('server'), options='-R')

    def makeclean(self):
        with settings(warn_only=True):
            sudo('rm -r {0}'.format(self.drush_makefile_dir()))

    def drupalsites(self):
        self.path_require(self.code_dir())

        # d7+ sites.php
        if self.d().drupal['write-aliases']:
            php = self.php_header()
            for site, c in self.d().drupal.sites.iteritems():
                s = dedent('''
                    $sites['{url}'] = '{site}';
                    ''')
                php += s.format(url=c.url, site=site)
            path = '{cd}/sites/sites.php'.format(cd=self.code_dir())
            put(StringIO.StringIO(php), path)

        for site, c in self.d().drupal.sites.iteritems():
            site_dir = '{b}/sites/{s}'.format(b=self.code_dir(), s=site)
            self.system().mkdir(site_dir)
            self.system().chmod(paths=site_dir, mode='770')

            # settings.php
            php = self.php_header()

            # source settings.php, we may need to reference source settings.php
            if self.s() and c.settings.include:

                # local path to the source settings.php
                if c.settings.include[:3] == '../':
                    path = os.path.normpath('{b}/{i}'.format(
                        b=site_dir, i=c.settings.include))
                elif c.settings.include[:1] == '/':
                    path = c.settings.include
                else:
                    path = '{b}/sites/{s}/{i}'.format(
                        b=self.code_dir(),
                        s=self.drupal_site_source_key(site),
                        i=c.settings.include)

                # remote path to the source settings.php
                if not files.exists(path):
                    src = os.path.normpath('{c}/sites/{s}/{i}'.format(
                        c=self.code_dir(ROLE_SRC),
                        s=self.drupal_site_source_key(site),
                        i=c.settings.include))
                    self.rsync(src, path)

                # @todo method to rsync other included settings files
                # if c.settings['nested-includes']

                # include source settings.php
                if c.settings.include:
                    s = dedent('''
                        require('{inc}');
                        ''')
                    php += s.format(inc=path)

            # files
            self.system().mkdir(self.assets_dir(site=site))

            # public
            path = self.drupal_assets_public_dir(site)
            self.system().mkdir(path)
            self.system().chmod(path, '775')
            link = '{b}/files'.format(b=site_dir)
            self.system().mkdir(os.path.dirname(link))
            with settings(warn_only=True):
                self.system().rm(link)
            self.system().link(target=path, link=link)
            # sylink source path to same
            if int(self.drupal_version()[0]) < 7:
                s = self.drush_vget(
                    'file_directory_path', ROLE_SRC,
                    self.drupal_site_source_key(site))
            else:
                s = self.drush_vget(
                    'file_public_path', ROLE_SRC,
                    self.drupal_site_source_key(site))
            link = '{b}/{s}'.format(b=self.code_dir(), s=s)
            self.system().mkdir(os.path.dirname(link))
            with settings(warn_only=True):
                self.system().rm(link)
            self.system().link(target=path, link=link)
            # configure
            if int(self.drupal_version()[0]) < 7:
                s = dedent('''
                    $conf['file_directory_path'] = 'sites/{site}/files';''')
            else:
                s = dedent('''
                    $conf['file_public_path'] = 'sites/{site}/files';''')
            php += s.format(site=site)

            # private
            if int(self.drupal_version()[0]) > 6:
                path = self.drupal_assets_private_dir(site)
                self.system().mkdir(path)
                self.system().chmod(path, '775')
                link = '{b}/private'.format(b=site_dir)
                self.system().mkdir(os.path.dirname(link))
                with settings(warn_only=True):
                    self.system().rm(link)
                self.system().link(target=path, link=link)
                # sylink source path
                s = self.drush_vget(
                    'file_private_path', ROLE_SRC,
                    self.drupal_site_source_key(site))
                if s:
                    link = '{b}/{s}'.format(b=self.code_dir(), s=s)
                    self.system().mkdir(os.path.dirname(link))
                    with settings(warn_only=True):
                        self.system().rm(link)
                    self.system().link(target=path, link=link)
                    # configure
                    s = dedent('''
                        $conf['file_private_path'] =
                            'sites/{site}/private';''')
                    php += s.format(site=site)

            # d8 configuration files directory
            if int(self.drupal_version()[0]) >= 8:
                path = self.drupal_assets_config_dir(site)
                paths = [
                    '{path}/active'.format(path=path),
                    '{path}/staging'.format(path=path),
                ]
                self.system().mkdir(paths)
                links = [
                    '{b}/config-active'.format(b=site_dir),
                    '{b}/config-staging'.format(b=site_dir),
                ]
                self.system().mkdir(
                    [os.path.dirname(links[0]), os.path.dirname(links[1])])
                with settings(warn_only=True):
                    self.system().rm(links)
                self.system().link(target=paths[0], link=links[0])
                self.system().link(target=paths[1], link=links[1])
                s = dedent('''
                    $config_directories = array(
                      CONFIG_ACTIVE_DIRECTORY => '{active}',
                      CONFIG_STAGING_DIRECTORY => '{staging}',
                    );
                    ''')
                php += s.format(active=links[0], staging=links[1])

            # path
            if c.settings.file:
                # write to configured path
                path = os.path.normpath('{b}/{inc}'.format(
                    b=site_dir, inc=c.settings.file))
            else:
                path = '{b}/settings.php'.format(b=site_dir)

            # db connection
            php += self.drupal_db_connection_php(c.db)

            # from conf
            if c.settings.php:
                php += "\n" + c.settings.php + "\n"

            # write
            if files.exists(path):
                self.system().chmod(paths=path, mode='666')
            elif not files.exists(os.path.dirname(path)):
                self.system().mkdir(os.path.dirname(path))
            put(StringIO.StringIO(php), path)
            self.system().chmod(paths=path, mode='440')
            self.system().chgrp(
                paths=os.path.dirname(path),
                group=self.system().group('server'))
            self.system().chmod(paths=os.path.dirname(path), mode='550')

    def drushaliases(self):
        # destination platform
        if 'value' in self.d().drupal.drush.alias:
            self.drush_alias_write(self.d().drupal.drush.alias)
        # destination sites
        for site in self.d().drupal.sites.itervalues():
            if 'value' in site.drush.alias:
                self.drush_alias_write(site.drush.alias)
        # source
        if self.s() is not None:
            if 'value' in self.s().drupal.drush.alias:
                self.drush_alias_write(self.s().drupal.drush.alias)
            for site in self.s().drupal.sites.itervalues():
                if 'value' in site.drush.alias:
                    self.drush_alias_write(site.drush.alias)

    def drushaliasesclean(self):
        for site in self.d().drupal.sites.itervalues():
            self.drush_alias_clean(site.drush.alias)
        if self.s():
            for site in self.s().drupal.sites.itervalues():
                self.drush_alias_clean(site.drush.alias)

    def filespull(self):
        for site, c in self.d().drupal.sites.iteritems():
            self.system().mkdir(self.assets_dir(site=site))

            # public
            if c.files.public.archive:
                src = '{a}/'.format(a=c.files.public.archive)
            else:
                src_site = self.drupal_site_source_key(site)
                src_code_dir = self.code_dir(ROLE_SRC)
                if int(self.drupal_version()[0]) < 7:
                    src = 'file_directory_path'
                else:
                    src = 'file_public_path'
                src = self.drush_vget(src, ROLE_SRC, src_site)
                if src is not None:
                    src = '{cd}/{files}/'.format(cd=src_code_dir, files=src)
            if src:
                dst = self.drupal_assets_public_dir(site)
                self.system().chown(
                    [dst], self.system().whoami(),
                    self.system().group('server'), options='-R')
                self.rsync(src, dst, excludes=c.files.public.exclude)
                self.system().chmod([dst], mode='774', options='-R')

            # private
            if int(self.drupal_version()[0]) >= 7:
                if c.files.private.archive:
                    src = '{a}/'.format(a=c.files.private.archive)
                else:
                    src_site = self.drupal_site_source_key(site)
                    src_code_dir = self.code_dir(ROLE_SRC)
                    src = self.drush_vget(
                        'file_private_path', ROLE_SRC, src_site)
                    if src:
                        src = '{cd}/{files}/'.format(
                            cd=src_code_dir, files=src)
                if src is not None:
                    dst = self.drupal_assets_private_dir(site)
                    self.system().chown(
                        [src, dst], self.system().whoami(),
                        self.system().group('server'), options='-R')
                    self.rsync(src, dst, excludes=c.files.private.exclude)
                    self.system().chmod([dst], mode='774', options='-R')

    def filesclean(self):
        with settings(warn_only=True):
            for site in self.d().drupal.sites.iterkeys():
                self.system().rm(
                    paths=[
                        '{cd}/sites/*/files'.format(cd=self.code_dir()),
                        self.assets_dir(site=site),
                    ],
                    options='-R')

    def virtualhost(self):
        # name for the virtualhost file
        name = self.virtualhost_name()
        if self.server().__class__.__name__ == 'Nginx':
            # /etc/hosts
            self.system().hosts_enable(self.hostnames())
            # directory structure, permissions
            self.system().mkdir(paths=[self.code_dir(), self.logs_dir()])
            self.system().chgrp(
                paths=[self.code_dir(), self.logs_dir()],
                group=self.system().group('server'))
            # write virtualhost conf file
            template = 'virtualhost.nginx.drupal{0}.j2'.format(int(
                self.drupal_version()[0]))
            template_path = pkg_resources.ResourceManager().resource_filename(
                __name__, 'assets')
            context = {
                'server_name': ' '.join(self.hostnames()),
                'root': self.code_dir(),
                'logs': self.logs_dir(),
            }
            self.system().virtualhost_nginx_enable(
                name, template, template_path, context)
        else:
            Dm.virtualhost(self)

    def virtualhostclean(self):
        # name for the virtualhost file
        name = self.virtualhost_name()
        if self.server().__class__.__name__ == 'Nginx':
            self.system().hosts_disable(self.hostnames())
            self.system().virtualhost_nginx_disable(name)
            with settings(warn_only=True):
                self.system().rm(paths=self.logs_dir(), options='-R')
        else:
            Dm.virtualhostclean(self)

    # utilities

    def assets_dir(self, role=None, site=None):
        path = self.base_dir('assets', role)
        if site is not None:
            path += '/{site}'.format(site=site)
        return path

    def drupal_assets_config_dir(self, site):
        return '{0}/{1}'.format(
            self.assets_dir(site=site), self.DRUPAL_ASSETS_CONFIG_DIR)

    def drupal_assets_public_dir(self, site):
        return '{0}/{1}'.format(
            self.assets_dir(site=site), self.DRUPAL_ASSETS_PUBLIC_DIR)

    def drupal_assets_private_dir(self, site):
        return '{0}/{1}'.format(
            self.assets_dir(site=site), self.DRUPAL_ASSETS_PRIVATE_DIR)

    def drupal_db_connection_php(self, conf):
        dv = self.drupal_version()
        php = ''
        if int(dv[0]) <= 6:
            for key, db in conf.iteritems():
                s = dedent("""
                    $db_url = 'mysqli://{user}:{pwd}@{host}/{db}';""")
                php += s.format(
                    key=urllib.quote_plus(key),
                    user=urllib.quote_plus(db.connection.username),
                    pwd=urllib.quote_plus(db.connection.password),
                    host=urllib.quote_plus(db.connection.host),
                    db=urllib.quote_plus(db.connection.database))
                if db.connection.prefix:
                    s = dedent("""
                        $db_prefix = {0};""")
                    if isinstance(db.connection.prefix, dict):
                        php += s.format(self.php_array(db.connection.prefix))
                    else:
                        php += s.format(db.connection.prefix)
            php += '\n'
        else:  # >= 7
            php = ''
            for key, db in conf.iteritems():
                s = dedent('''
                    $databases['{key}']['default'] = {hash};
                    ''')
                php += s.format(key=key, hash=self.php_array(db.connection))
        return php

    def drupal_site_source_key(self, site):
        '''
        Gets the name of the source site directory for the given dest site.'''
        if self.s() is not None:
            src_sites = self.s().drupal.sites
            dst_sites = self.d().drupal.sites
            # configured/cached
            if dst_sites[site].source:
                return dst_sites[site].source
            # single sites dir in each platform
            if len(src_sites) == 1 and len(dst_sites) == 1:
                for src in src_sites.iterkeys():
                    return src
            # both platforms use same name
            for src in src_sites.iterkeys():
                if src in dst_sites:
                    return src
        return None

    def drupal_site_source_conf(self, site):
        key = self.drupal_site_source_key(site)
        if key and key in self.s().drupal.sites:
            return self.s().drupal.sites[key]
        return None

    def drupal_version(self):
        if self._drupal_version is not None:
            return self._drupal_version
        # configured
        if self.d().drupal.version is not None:
            self._drupal_version = str(self.d().drupal.version).split('.')
            return self._drupal_version
        # try to use drush status
        self.path_require(self.code_dir())
        with settings(warn_only=True):
            status = self.drush_status()
        if status is not None:
            if 'drupal-version' in status:
                self._drupal_version = status['drupal-version'].split('.')
                return self._drupal_version
            elif 'drupal_version' in status:
                self._drupal_version = status['drupal_version'].split('.')
                return self._drupal_version
        # try to use git
        with settings(warn_only=True):
            branch = self.scm_git_branch_get(self.code_dir())
            if branch is not None and re.match(r'^[1-9]+\.', branch):
                self._drupal_version = branch.split('.')
                return branch
        abort('Could not determine drupal version.')

    def drush(self, dst=True):
        return self.p().drupal.drush.bin

    def drush_alias_exists(self, alias):
        existing = run('{drush} sa'.format(drush=self.drush())).strip().split()
        # older drush versions returned the @ symbol
        map(lambda a: a if a[0] != '@' else a[1:], existing)
        return alias in existing

    def drush_alias_write(self, c):
        path = "{home}/.drush/{name}.alias.drushrc.php".format(
            home=self.system().home_dir(), name=c.name)
        self.system().mkdir(paths=os.path.dirname(path))
        php = self.php_header() + dedent('''
            $aliases['{name}'] = {value};
            ''')
        php = php.format(name=c.name, value=self.php_array(c.value))
        put(StringIO.StringIO(php), path)

    def drush_alias_clean(self, c):
        path = '{home}/.drush/{name}.alias.drushrc.php'.format(
            home=self.system().home_dir(), name=c.name)
        with settings(warn_only=True):
            self.system().rm(path)

    def drush_makefile_dir(self, role=None):
        '''The platform assets directory'''
        if role == ROLE_SRC:
            abort('source makefile directory not know-able.')
        return '/'.join(
            [self.assets_dir(), self.DRUPAL_ASSETS_MAKEFILE_DIR])

    def drush_makefile_checkout(self, conf):
        mf_dir = self.drush_makefile_dir()
        if files.exists(mf_dir):
            warn('make file directory "{p}" exists, skipping checkout.'.format(
                p=mf_dir))
        else:
            if conf.type == 'git':
                self.scm_git_clone(
                    conf.url, mf_dir,
                    conf.branch if 'branch' in conf else None,
                    conf.tag if 'tag' in conf else None)
            else:
                abort('scm type "{0}" not supported.'.format(conf.type))

    def drush_db_pull(self, dst_alias, src_alias, db_key='default',
                      options=[]):
        cmd_sync = '{drush} sql-sync -y ' + \
            ' '.join(options['sql-sync']) + ' ' + \
            '--source-database={db_key} ' + \
            '--target-database={db_key} ' + \
            '@{src} @{dst}'
        run(cmd_sync.format(drush=self.drush(), db_key=db_key, src=src_alias,
            dst=dst_alias))

    def drush_run(self, args='', role=ROLE_DST, site=None):
        if site is None:
            cmd = "{drush} -y -r {r} {args}".format(
                drush=self.drush(), r=self.code_dir(role), args=args)
        else:
            if role == ROLE_SRC:
                alias = self.s().drupal.sites[site].drush.alias.name
            else:
                if ROLE_DST not in env.roles:
                    abort('attempting to run drush @dst on src')
                alias = self.d().drupal.sites[site].drush.alias.name
            if alias is not None:
                cmd = "{drush} -y @{a} {args}".format(
                    drush=self.drush(),
                    a=alias, args=args)
            else:
                cmd = "{drush} -y -r {r} -l {l} {args}".format(
                    drush=self.drush(), r=self.code_dir(role),
                    l=site, args=args)
        result = run(cmd)
        if result.failed:
            return None
        # remove PHP notices, etc.
        lines = result.strip().split('\n')
        while len(lines) and lines[0][:4] == 'PHP ':
            lines = lines[1:]
        return '\n'.join(lines)

    def drush_version(self, role=None, site=None):
        if self._drush_version is None:
            v = self.drush_run('--version', role, site)
            for line in v.strip().split('\n'):
                if ':' in line:
                    self._drush_version = line.split(':')[1].strip()
                elif 'drush version ' in line:
                    self._drush_version = line.split(
                        'drush version ')[1].strip()
                else:
                    abort('Could not determine drush version.')
        return self._drush_version

    def drush_status(self, role=ROLE_DST, site=None, key=None):
        site_key = '_' if site is None else site
        role_status = self._drush_status[role]
        if site_key not in role_status:
            status = self.drush_run("core-status --pipe", role, site)
            if status is None:
                return None
            if int(self.drush_version(role, site).split('.')[0]) < 7:
                role_status[site_key] = {}
                for item in status.strip().split('\n'):
                    if item.find('=') > -1:
                        k, v = item.split('=')
                        role_status[site_key][k.strip()] = v.strip()
            else:
                role_status[site_key] = json.loads(status)
        if key is not None:
            if key in role_status[site_key]:
                return role_status[site_key][key]
            else:
                return None
        return role_status[site_key]

    def drush_vget(self, name, role=None, site=None):
        if name not in self._drush_vars:
            cmd = 'variable-get --format=json {0}'
            result = self.drush_run(cmd.format(name), role, site)
            try:
                val = json.loads(result)
            except:
                # some older versions of drush return values like
                #   file_public_path: "sites/default/files"
                result = re.sub(r'^([^":]+):', r'"\1":', result.strip())
                val = json.loads('{ ' + result + ' }')
            if isinstance(val, basestring):
                self._drush_vars[name] = val
            elif name not in val:
                abort('Could not get drupal variable "{0}"'.format(name))
            else:
                self._drush_vars[name] = val[name]
        return self._drush_vars[name]

    def drush_vset(self, name, value, role=None, site=None):
        self._drush_vars[name] = value
        cmd = 'variable-set --exact --yes --format=json {0} {1}'
        return self.drush_run(cmd.format(name, json.dumps(value)), role, site)

    def hostnames(self, role=ROLE_DST):
        c = self.s() if role == ROLE_SRC else self.d()
        hostnames = []
        for site in c.drupal.sites.itervalues():
            hostnames.append(site.url)
        return hostnames
