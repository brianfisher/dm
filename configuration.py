from fabric.api import abort
from lya import AttrDict
from copy import copy


class Configuration():
    '''
    Manages a Dm configuration dict.

    Supports a nested dict with leaves of type

    -   scaler types
    -   lists with scaler values.

    Does not support lists of dicts.
    '''

    # the configuration AttrDict
    c = None
    # the current task (for abort message)
    _task = None

    def __init__(self, d):
        self.c = AttrDict(d)

    def get(self, path=[], default=None):
        '''
        Gets the value at path, ensuring the path exists, and writing the
        default if missing.
        '''
        return self._get(self.c, path, default)

    def _get(self, d, path, default=None):
        '''
        Gets the value in d at path, ensuring the path exists, and writing the
        default if missing.
        '''
        nones = [None, {}, []]
        q = list(path)
        q.reverse()
        key = q.pop() if len(q) else None
        while key is not None:
            if key not in d:
                d[key] = {} if len(q) else default
            # treat an empty leaf as "missing" when there is a
            # default value
            elif len(q) == 0 and default is not None and d[key] in nones:
                d[key] = default
            d = d[key]
            key = q.pop() if len(q) else None
        return d

    def set_src(self, key):
        if key is None:
            self.c._dm.src = None
            self.c._dm.src_key = None
            return
        if key not in self.c.platforms:
            abort('No /platforms/"{p}" found.'.format(p=key))
        self.c._dm.src = self.c.platforms[key]
        self.c._dm.src_key = key

    def src(self):
        '''Return the configuration dict for the source platform.'''
        return self.c._dm.src

    def src_key(self):
        '''Return the configuration dict key for the source platform.'''
        return self.c._dm.src_key

    def set_dst(self, key):
        if key not in self.c.platforms:
            abort('No /platforms/"{p}" found.'.format(p=key))
        self.c._dm.dst = self.c.platforms[key]
        self.c._dm.dst_key = key

    def dst(self):
        '''Return the configuration dict for the destination platform.'''
        return self.c._dm.dst

    def dst_key(self):
        '''Return the configuration dict key for the destination platform.'''
        return self.c._dm.dst_key

    def error(self, path, message):
        msg = '\n'
        if self._task:
            msg += 'task: {t}\n'.format(t=self._task)
        msg += 'path: {p}\n'.format(p=path)
        msg += 'error: {m}\n'.format(m=message)
        abort(msg)

    def dst_error(self, paths, message):
        paths_str = map(lambda p: '{r}->{p}'.format(
            r='->'.join(['platforms', self.dst_key()]),
            p='->'.join(p)), paths)
        if len(paths) > 1:
            paths_str = 'At least one of\n\t' + '\n\t'.join(paths_str)
        self.error(paths_str, message)

    def src_error(self, paths, message):
        paths_str = map(lambda p: '{r}->{p}'.format(
            r='->'.join(['platforms', self.src_key()]),
            p='->'.join(p)), paths)
        if len(paths) > 1:
            paths_str = 'At least one of\n\t' + '\n\t'.join(paths_str)
        self.error(paths_str, message)

    # rebasing, validation, inheritance

    def task(self, name=None):
        '''Sets the name of the current task for abort messages.'''
        self._task = name
        return self

    def require(self, d, paths):
        '''Require at least one of paths in dict d, abort if missing.'''
        fail = True
        for path in paths:
            if self._get(d, path) is not None:
                fail = False
                break
        if fail:
            paths_str = map('->'.join, paths)
            if len(paths) > 1:
                paths_str = 'At least one of\n\t' + '\n\t'.join(paths_str)
            self.error(paths_str, 'Path not found.')
        return self

    def dst_require(self, paths):
        '''Require at least one of destination paths, abort if missing.'''
        fail = True
        for path in paths:
            if self._get(self.dst(), path) is not None:
                fail = False
                break
        if fail:
            self.dst_error(paths, 'Path not found.')
        return self

    def src_require(self, paths):
        '''Require at least one of source paths, abort if missing.'''
        fail = True
        for path in paths:
            if self._get(self.src(), path) is not None:
                fail = False
                break
        if fail:
            self.src_error(paths, 'Path not found.')
        return self

    def rebase(self, d, path, default):
        '''Rebase a path in dict d onto default values.'''
        value = self._get(d, path, {})
        # use AttrDict.rebase() for dict's
        if isinstance(value, dict):
            if not isinstance(value, AttrDict):
                value = AttrDict(value)
            value.rebase(default)
        elif not value:
            value = default
        # above rebase() will not set empty lists, dicts
        if isinstance(default, dict):
            for k, v in default.iteritems():
                if k not in value:
                    value[k] = copy(v)
        return self

    def dst_rebase(self, path, default):
        '''Rebase destination path onto default values.'''
        self.rebase(self.dst(), path, default)
        return self

    def src_rebase(self, path, default):
        '''Rebase source path onto default values if source is set.'''
        if self.src():
            self.rebase(self.src(), path, default)
        return self

    def inherit(self, path):
        '''Inherit the destination path from source if missing.'''
        default = None
        if self.src():
            default = self._get(self.src(), path)
        self._get(self.dst(), path, default)
        return self

    def validate(self, d, path, function, *args):
        '''
        Validate the path with the function. If the function returns a string,
        will abort and print that string.
        '''
        message = function(self._get(d, path, None), args)
        if message:
            self.error('->'.join(path), message)

    def dst_validate(self, path, function, *args):
        '''
        Validate the destination path with the function. If the function
        returns a string, will abort and print that string.
        '''
        message = function(self._get(self.dst(), path, None), args)
        if message:
            self.dst_error(path, message)

    def src_validate(self, path, function, *args):
        '''
        Validate the source path with the function. If the function returns a
        string, will abort and print that string.
        '''
        message = function(self._get(self.src(), path, None), args)
        if message:
            self.src_error(path, message)
