from fabric.api import env
from dm.drupal import Drupal

# tasks

from dm import (
    configure, configureclean, ls, status, roles,
    virtualhostclean,
    codecheckout, codeclean)
from dm.drupal import (
    drushaliases, drushaliasesclean,
    drupalsites,
    virtualhost,
    filespull, filesclean,
    dbpull, dbclean)

# custom tasks

from fabric.api import execute
from dm import Clean, Make
from dm.drupal import Pull


class MyClean(Clean):
    '''Deletes code, db, files, virtualhost, drush aliases.'''

    def run(self):
        '''
        Alias for "virtualhost-clean code-clean db-clean
        files-clean drush-aliases-clean".'''
        execute(virtualhostclean)
        execute(codeclean)
        execute(dbclean)
        execute(filesclean)
        execute(drushaliasesclean)

clean = MyClean()


class MyMake(Make):
    '''Builds and configures the drupal codebase and virtualhost.'''

    def run(self):
        '''
        Alias for "code-checkout drupal-sites drush-aliases virtualhost"
        '''
        execute(drushaliases)
        execute(codecheckout)
        execute(drupalsites)
        execute(virtualhost)

make = MyMake()


class MyPull(Pull):
    '''Copies database from source to destination.'''

    def run(self):
        '''Alias for "db-pull".'''
        #execute(filespull)
        execute(dbpull)

pull = MyPull()

# forward ssh keys to destination so it can ssh to source
env.forward_agent = True

# instantiate the dm platform object and save to fabric env variable.
env.dm = Drupal(
    # configuration files, latter overwrites former
    path=[
        'conf.yaml',
        # 'conf_local.yaml',
    ],
    # all tasks
    tasks=[
        # dm
        configure, configureclean, ls, status, roles,
        virtualhostclean,
        codecheckout, codeclean,
        # dm.drupal
        drushaliases, drushaliasesclean,
        drupalsites,
        virtualhost,
        filespull, filesclean,
        dbpull, dbclean,
        # custom
        clean, make, pull,
    ])
