# DM

This is a [Fabric](fabfile.org) script that automates the process of mirroring a website for development. It currently supports websites built using Drupal.

## Features

* Simple workflow -- one command configures a destination and source site, a second builds and populates the destination site.
* Production sites are configured as such, and can not be modified.
* Works with remote and/or local sites via ssh.
* Configurable -- all configuration is in yaml, and can be broken up into multiple files.
* Extendable -- The current feature set can be extended to different platforms, hosts, and frameworks.

## Installation

This script can be installed anywhere that has ssh access to the destination site, or on the destination itself.

Install Python 2.7 and the following packages

* Fabric 1.4.1+
* layered-yaml-attrdict-config
* Jinja2

Create a directory for your script, and checkout this project to a subdirectory named "dm", e.g.

    git clone git@bitbucket.org:brianfisher/dm.git

## Configuration

Create a fabfile and a configuration file from the examples provided.

    cp dm/EXAMPLE_conf.yaml conf.yaml
    cp dm/EXAMPLE_fabfile.py fabfile.py

See the yaml files in `dm/conf/` for documentation of configuration options.

## Usage

To see a list of available commands

    fab -l

Configure a build by specifying a destination and source

    fab configure:source=prod,dest=local-stage

then build the environment, pull content, and configure a virtualhost

    fab make pull virtualhost
