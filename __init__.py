from fabric.api import abort, cd, env, execute, run, settings, sudo, warn
from fabric.contrib import files
from configuration import Configuration
from tasks import Task
import os
from lya import AttrDict
import re
import sys
import pkg_resources
from textwrap import dedent
from isa import isstring

# fabric role keys
ROLE_SRC = 'src'
ROLE_DST = 'dst'

# task configuration states
CONFIGURE_QUEUED = 0
CONFIGURE_WAIT = 1
CONFIGURE_COMPLETE = 2

# fabric commands


class DmTask(Task):
    '''Configuration routines shared by tasks.'''

    def validate_path(self, value, args):
        filepath = args[0]
        if filepath[0] != '/':
            return 'filepath "{0}" is not absolute.'.format(filepath)


class Configure(DmTask):
    '''Define local and source, do any initial configuration.'''
    name = 'configure'
    role = None

    def run(self, dest, source=None):
        '''
        Define the local and source sites and process any initial environment
        configuration.

        Arguments are both platform keys from the conf files.

        Keyword arguments:
        dest -- The site to create.
        source -- (optional) The site to clone.'''
        env.dm.conf_configure(dest, source)

configure = Configure()


class ConfigureClean(DmTask):
    '''Flush the configuration cache.'''
    name = 'configure-clean'
    role = None

    def run(self):
        env.dm.conf_clean()

configureclean = ConfigureClean()


class Ls(DmTask):
    '''Print configured platforms.'''
    name = 'ls'
    role = None

    def run(self):
        '''Print configured platforms.'''
        env.dm.ls()

ls = Ls()


class Roles(DmTask):
    '''Print the current source and destination.'''
    name = 'roles'
    role = None

    def run(self):
        '''Describe the current environment.

        Keyword arguments:
        conf -- print the full configuration loaded from yaml files.'''
        env.dm.roles()

roles = Roles()


class Status(DmTask):
    '''Describe the current environment.'''
    name = 'status'
    role = None

    def run(self, conf=False):
        '''Describe the current environment.

        Keyword arguments:
        conf -- print the full configuration loaded from yaml files.'''
        env.dm.status(self.arg_bool(conf, False))

status = Status()


class VirtualHost(DmTask):
    '''Configure a virtualhost for the destination site.'''
    name = 'virtualhost'

    def run(self):
        '''Configure a virtualhost for the destination site.'''
        env.dm.virtualhost()

    def configure(self):
        DmTask.configure(self)

    def configure_values(self, key):
        if key == 'type':
            return ['apache2']

virtualhost = VirtualHost()


class VirtualHostClean(DmTask):
    '''Remove the virtualhost for the destination site.'''
    name = 'virtualhost-clean'

    def run(self):
        env.dm.virtualhostclean()

virtualhostclean = VirtualHostClean()


class CodeCheckout(DmTask):
    '''Checkout the codebase from a scm.'''
    name = 'code-checkout'

    def run(self):
        env.dm.code_checkout()

    def configure(self):
        DmTask.configure(self)
        cnf = env.dm.conf()

        path = ['path', 'code']
        (cnf.dst_require([path])
            .dst_validate(path, self.validate_path, env.dm.code_dir()))

        path = ['repo']
        cnf.inherit(path)
        default = env.dm.yaml_pkg_load('conf/dm-platform-repo.yaml')
        for dest in env.dm.d().repo.iterkeys():
            (cnf.dst_rebase(path + [dest], default)
                .dst_require([path + [dest, 'type']])
                .dst_require([path + [dest, 'url']]))

        path = ['link']
        cnf.inherit(path)
        default = env.dm.yaml_pkg_load('conf/dm-platform-link.yaml')
        for dest in env.dm.d().link.iterkeys():
            cnf.dst_require([path + [dest, 'target']])

codecheckout = CodeCheckout()


class CodeClean(DmTask):
    '''Delete the drupal codebase.'''
    name = 'code-clean'

    def run(self):
        env.dm.code_clean()

codeclean = CodeClean()


class CodePull(DmTask):
    '''Rsync the codebase from a source.'''
    name = 'code-pull'

    def run(self):
        env.dm.code_pull()

    def configure(self):
        DmTask.configure(self)
        cnf = env.dm.conf()

        cnf.dst_require([['path']])
        c = env.dm.d().path
        if not c.base and not c.code:
            abort('one of path/base or path/code for destination not found.')
        c = env.dm.s().path
        if not c.base and not c.code:
            abort('one of path/base or path/code for source not found.')

codepull = CodePull()


class Clean(DmTask):
    '''Alias for "virtualhost-clean code-clean configure-clean".'''
    name = 'clean'
    role = None

    def run(self):
        execute(virtualhostclean)
        execute(codeclean)
        execute(configureclean)

clean = Clean()


class Make(DmTask):
    '''Alias for "code-checkout"'''
    name = 'make'
    role = None

    def run(self):
        execute(codecheckout)

make = Make()

# Dm base classes


class System():
    '''
    Dm abstract base class for an operating system.
    '''

    sudo = True

    def run(self, command, shell=True, pty=True, combine_stderr=True,
            user=None):
        if self.sudo:
            return sudo(command, shell, pty, combine_stderr, user)
        else:
            return run(command, shell, pty, combine_stderr)

    def chgrp(self, paths, group, options=''):
        raise NotImplementedError

    def chown(self, paths, user, group=None, options=''):
        raise NotImplementedError

    def chmod(self, paths, mode, options=''):
        raise NotImplementedError

    def home_dir(self, user=None):
        raise NotImplementedError

    def hosts_enable(self, hostnames):
        raise NotImplementedError

    def hosts_disable(self, hostnames):
        raise NotImplementedError

    def link(self, target, link):
        raise NotImplementedError

    def mkdir(self, path):
        raise NotImplementedError

    def rm(self, paths, options=''):
        raise NotImplementedError

    def rsync(self, src, dst, port=None, options=[]):
        raise NotImplementedError

    def whoami(self):
        raise NotImplementedError

    # system-specific application management

    def virtualhost_apache_enable(self, name, template, template_path,
                                  context):
        raise NotImplementedError

    def virtualhost_apache_disable(self, name):
        raise NotImplementedError

    def virtualhost_nginx_enable(self, name, template, template_path, context):
        raise NotImplementedError

    def virtualhost_nginx_disable(self, name):
        raise NotImplementedError


class Unix(System):
    '''
    Dm abstract base class for a Unix operating system.
    '''

    def chgrp(self, paths, group, options=''):
        if isstring(paths):
            paths = [paths]
        cmd = 'chgrp {options} {group} {paths}'.format(
            options=options, group=group, paths=' '.join(paths))
        return self.run(cmd)

    def chown(self, paths, user, group=None, options=''):
        mode = user
        if group is not None:
            mode += ':{group}'.format(group=group)
        if isstring(paths):
            paths = [paths]
        cmd = 'chown {options} {mode} {paths}'.format(
            options=options, mode=mode, paths=' '.join(paths))
        return self.run(cmd)

    def chmod(self, paths, mode, options=''):
        if isstring(paths):
            paths = [paths]
        cmd = 'chmod {options} {mode} {paths}'.format(
            options=options, mode=mode, paths=' '.join(paths))
        return self.run(cmd)

    def group(self, group_type='server'):
        if group_type == 'server':
            return 'www-data'
        return None

    def home_dir(self, user=None):
        if user is None:
            user = self.whoami()
        return run('echo ~{user}'.format(user=user)).strip()

    def hosts_enable(self, hostnames):
        # ip address
        cmd = "ifconfig eth1 | " + \
              "grep 'inet addr:' | " + \
              "sed -r 's/^.*inet addr:([^ ]+).*$/\\1/'"
        ip = self.run(cmd).strip()
        # ip = '127.0.0.1'
        # hosts file
        dns = "{0} {1} # generated by dm https://bitbucket.org/brianfisher/dm"
        dns = dns.format(ip, ' '.join(hostnames))
        files.append(filename='/etc/hosts', text=dns, use_sudo=self.sudo)
        # host system configuration notice
        msg = dedent('''
        IMPORTANT: Add

        {0} {1}

        to your host system hosts file.''').strip()
        print(msg.format(ip, ' '.join(hostnames)))

    def hosts_disable(self, hostnames):
        files.sed(
            filename='/etc/hosts',
            before='^.* ({0}) # generated by dm .*$'.format(
                '|'.join(hostnames)),
            after='',
            use_sudo=self.sudo)

    def link(self, target, link):
        return run('ln -s {target} {link}'.format(target=target, link=link))

    def mkdir(self, paths, group=None):
        if isstring(paths):
            paths = [paths]
        if group is None:
            group = self.group()
        run('mkdir -p {0}'.format(' '.join(paths)))
        self.chmod(paths, 'g+s')
        if group is not None:
            self.chgrp(paths, group)

    def rm(self, paths, options=''):
        if isstring(paths):
            paths = [paths]
        cmd = 'rm {options} {paths}'.format(
            options=options, paths=' '.join(paths))
        return self.run(cmd)

    def rsync(self, src, dst, port=None, options=[]):
        if port:
            options.append("-e 'ssh -p {0}'".format(port))
        cmd = 'rsync {options} {src} {dst}'.format(
            options=' '.join(options), src=src, dst=dst)
        return run(cmd)

    def whoami(self):
        return run('whoami').strip()


class Application():
    '''
    Dm abstract base class for an application.
    '''

    # components
    system = System()

    def __init__(self, system=None):
        '''Constructor

        Keyword arguments:
        system -- a dm.System object'''
        if system is not None:
            self.system = system


class Server(Application):
    '''
    Dm abstract base class for a web server.
    '''


class Database(Application):
    '''
    Dm abstract base class for a sql application.
    '''

    def databases(self, conn):
        raise NotImplementedError

    def execute(self, conn, sql=None, path=None):
        raise NotImplementedError


class Dm():
    '''
    Dm abstract base class for a website development platform.
    '''

    # configuration cache
    CONF_CACHE_FILENAME = os.path.expanduser('~/.dm/conf')

    # the user specified configuration
    _conf = None
    conf_path = None

    # tasks
    tasks = None
    configured = None

    # components
    _system = None
    _server = None
    _db = None

    RSYNC_OPT_DEFAULT = [
        '--copy-links',
        '--delete',
        '--recursive',
        '--times',
        '--compress',
        '--human-readable',
        '--verbose',
    ]

    def __init__(self, path, tasks):
        '''
        Constructor

        Keyword arguments:
        path -- list of configuration file paths. Latter overwrites former.
        tasks -- allowed tasks, as a list of dm.tasks.Task objects'''
        self.tasks = tasks
        self.conf_path = path
        if os.path.exists(self.CONF_CACHE_FILENAME):
            self._conf = Configuration(AttrDict.from_yaml(
                self.CONF_CACHE_FILENAME))
            self.roledefs_set()

    # components

    def system(self):
        '''gets the system object for the current role.'''
        if len(env.roles) == 0:
            return None
        if self._system is None:
            src = None
            if self.s():
                src = self.get_object(self.s().system['class'])
                if 'sudo' in self.s().system:
                    src.sudo = self.s().system.sudo
            dst = self.get_object(self.d().system['class'])
            if 'sudo' in self.d().system:
                dst.sudo = self.d().system.sudo
            self._system = {
                ROLE_SRC: src,
                ROLE_DST: dst,
            }
        return self._system[env.roles[0]]

    def server(self):
        '''gets the server object for the current role.'''
        if len(env.roles) == 0:
            return None
        if self._system is None:
            self.system()
        if self._server is None:
            self._server = {
                ROLE_SRC: self.get_object(
                    self.s().server['class'],
                    self._system[ROLE_SRC]) if self.s() else None,
                ROLE_DST: self.get_object(
                    self.d().server['class'], self._system[ROLE_DST]),
            }
        return self._server[env.roles[0]]

    def db(self):
        '''gets the database object for the current role.'''
        if len(env.roles) == 0:
            return None
        if self._system is None:
            self.system()
        if self._db is None:
            self._db = {
                ROLE_SRC: self.get_object(
                    self.s().database['class'],
                    self._system[ROLE_SRC]) if self.s() else None,
                ROLE_DST: self.get_object(
                    self.d().database['class'], self._system[ROLE_DST]),
            }
        return self._db[env.roles[0]]

    def get_object(self, classname, *args, **kwargs):
        '''Instantiates a class classname.'''
        _module = __import__('dm.{0}'.format(classname.lower()))
        _module = getattr(_module, classname.lower())
        _class = getattr(_module, classname)
        return _class(*args, **kwargs)

    # configuration

    def conf(self):
        '''Get the Configuration object.'''
        if self._conf is None:
            abort("Need to run configure.")
        return self._conf

    def c(self, path=None):
        '''
        Return the full configuration dictionary.

        Keyword arguments:
        path -- List of keys to walk
        '''
        if not path:
            return self.conf().get()
        return self.conf().get(path)

    def p(self):
        '''
        Return the configuration dictionary for the current task platform.
        '''
        if len(env.roles) != 1:
            abort('number of roles not 1.')
        if ROLE_DST in env.roles:
            return self.d()
        if ROLE_SRC in env.roles:
            return self.s()
        abort('no role set.')

    def s(self):
        '''Gets configuration for the source platform.'''
        return self.conf().src()

    def d(self):
        '''Gets configuration for the destination platform.'''
        return self.conf().dst()

    def configure(self, dest, source=None):
        # set the source and destination
        default = self.yaml_pkg_load('conf/dm-platform.yaml')
        self.conf().set_src(source)
        if source is not None:
            self.conf().src_rebase([], default)
        self.conf().set_dst(dest)
        self.conf().dst_rebase([], default)

        # bail if destination is protected
        if self.d().protect:
            abort('can not set destination platform when /protect is "yes"')

        # platform config
        self.conf().task('configure')
        (self.conf()
            .inherit(['system', 'sudo'])
            .inherit(['system', 'class'])
            .dst_require([['system', 'class']])
            .inherit(['server', 'class'])
            .dst_require([['server', 'class']])
            .inherit(['database', 'class'])
            .dst_require([['database', 'class']]))

        # set the fabric hosts via roles
        self.roledefs_set()

    def conf_load(self):
        # load user config
        self._conf = Configuration(AttrDict.from_yaml(self.conf_path[0]))
        for i in range(1, len(self.conf_path)):
            self.conf().get().update_yaml(self.conf_path[i])
        # defaults
        self.conf().rebase(
            self.conf().get(), [], self.yaml_pkg_load('conf/dm.yaml'))
        # check top level items
        (self.conf().task('configure')
            .require(self.conf().get(), [['name']])
            .require(self.conf().get(), [['shortname']]))

    def conf_save(self):
        if not os.path.exists(os.path.dirname(self.CONF_CACHE_FILENAME)):
            os.makedirs(os.path.dirname(self.CONF_CACHE_FILENAME))
        with open(self.CONF_CACHE_FILENAME, 'w') as f:
            self.conf().get().dump(f)

    def conf_tasks(self, tasks=None):
        '''Runs configure() on all enabled tasks.'''
        # allow recursion with initial parameterless call
        if tasks is None:
            tasks = self.tasks
        # hash of configured state
        if self.configured is None:
            self.configured = dict(
                (task.name, CONFIGURE_QUEUED) for task in self.tasks)
        for task in tasks:
            # enabled
            if task.name not in self.configured:
                abort('task named {t} not enabled'.format(t=task.name))
            # circular dependencies
            if self.configured[task.name] == CONFIGURE_COMPLETE:
                continue
            if self.configured[task.name] == CONFIGURE_WAIT:
                abort('circular dependency detected for task named {t}'.format(
                    t=task.name))
            # configure dependencies
            self.configured[task.name] = CONFIGURE_WAIT
            dependencies = []
            for classname in task.dependencies:
                dep_task = self.conf_tasks_get_by_classname(classname)
                if dep_task is None:
                    abort('task class {t} not enabled'.format(t=task.name))
                dependencies.append(dep_task)
            self.conf_tasks(dependencies)
            # configure
            task.configure()
            self.configured[task.name] = CONFIGURE_COMPLETE

    def conf_tasks_get_by_classname(self, classname):
        for task in self.tasks:
            if isinstance(task, classname):
                return task
        return None

    # tasks

    def conf_configure(self, dest, source=None):
        self.conf_load()
        self.configure(dest=dest, source=source)
        self.conf_tasks()
        self.conf_save()

    def conf_clean(self):
        if os.path.exists(self.CONF_CACHE_FILENAME):
            os.remove(self.CONF_CACHE_FILENAME)

    def code_checkout(self):
        for dest, repo in self.d().repo.iteritems():
            dest = self.base_dir(dest)
            if files.exists(dest):
                warn('directory "{p}" exists, skipping checkout.'.format(
                    p=dest))
                continue
            self.system().mkdir(dest)
            if repo.type == 'git':
                self.scm_git_clone(
                    repo.url, dest,
                    repo.branch if 'branch' in repo else None,
                    repo.tag if 'tag' in repo else None)
            else:
                abort('scm type "{0}" not supported.'.format(repo.type))
        for dest, link in self.d().link.iteritems():
            dest = self.base_dir(dest)
            if files.exists(dest):
                self.system().rm(dest, '-r')
            self.system().link(link.target, dest)

    def code_clean(self):
        with settings(warn_only=True):
            for dest in self.d().repo.iterkeys():
                path = self.base_dir(dest)
                if files.exists(path):
                    self.system().rm(path, '-r')
            for dest in self.d().link.iterkeys():
                path = self.base_dir(dest)
                if files.exists(path):
                    self.system().rm(path)
            self.system().rm(self.code_dir(), '-r')

    def code_pull(self):
        src = self.code_dir(ROLE_SRC) + '/'
        dst = self.code_dir()
        self.system().mkdir(dst)
        self.rsync(src, dst)

    def ls(self):
        for platform in self.c().platforms.iterkeys():
            print(platform)

    def roles(self):
        source = self.role_key(ROLE_SRC)
        if source:
            print('Source:      {key}'.format(key=source))
        print('Destination: {key}'.format(
            key=self.role_key()))

    def status(self, conf):
        if conf:
            print('Loaded configuration:\n')
            self.c().dump(sys.stdout)
        else:
            print('Calculated configuration:\n')
            s = self.s()
            if s:
                self.status_header('Source "{key}"'.format(
                    key=self.role_key(ROLE_SRC)))
                s.dump(sys.stdout)
            else:
                self.status_header('Source')
                print('No source configured.')
            self.status_header('Destination "{key}"'.format(
                key=self.role_key()))
            self.d().dump(sys.stdout)

    def virtualhost(self):
        name = self.virtualhost_name()
        if self.server().__class__.__name__ == 'Apache':
            # /etc/hosts
            self.system().hosts_enable(self.hostnames())
            # directory structure, permissions
            self.system().mkdir(paths=[self.code_dir(), self.logs_dir()])
            self.system().chgrp(
                paths=[self.code_dir(), self.logs_dir()],
                group=self.system().group('server'))
            # write virtualhost conf file
            if self.system().__class__.__name__ == 'Centos':
                template = 'virtualhost.apache2.centos.j2'
            else:
                template = 'virtualhost.apache2.j2'
            template_path = pkg_resources.ResourceManager().resource_filename(
                __name__, 'assets')
            context = {
                'server_name': ' '.join(self.hostnames()),
                'root': self.code_dir(),
                'logs': self.logs_dir(),
                'secure': True,
                'insecure': True,
            }
            self.system().virtualhost_apache_enable(
                name, template, template_path, context)
        else:
            raise NotImplementedError

    def virtualhostclean(self):
        name = self.virtualhost_name()
        if self.server().__class__.__name__ == 'Apache':
            self.system().hosts_disable(self.hostnames())
            self.system().virtualhost_apache_disable(name)
            with settings(warn_only=True):
                self.system().rm(paths=self.logs_dir(), options='-R')
        else:
            raise NotImplementedError

    # utilities

    def base_dir(self, path='', role=ROLE_DST):
        '''Append path to base path if relative.'''
        c = self.s() if role == ROLE_SRC else self.d()
        ret = []
        if c.path.base and (not path or path[0] != '/'):
            ret.append(c.path.base)
        if path:
            ret.append(path)
        ret = os.path.normpath('/'.join(ret))
        return ret

    def assets_dir(self, role=None):
        '''The platform assets directory'''
        c = self.s() if role == ROLE_SRC else self.d()
        return self.base_dir(c.path['assets'], role)

    def code_dir(self, role=None):
        '''The platform code directory'''
        c = self.s() if role == ROLE_SRC else self.d()
        return self.base_dir(c.path['code'], role)

    def logs_dir(self, role=None):
        '''The platform logs directory'''
        c = self.s() if role == ROLE_SRC else self.d()
        return self.base_dir(c.path['logs'], role)

    def tmp_dir(self, role=ROLE_DST):
        c = self.s() if role == ROLE_SRC else self.d()
        return self.base_dir(c.path['tmp'], role)

    def hostnames(self, role=None):
        '''Gets a list of all hostnames for the platform.'''
        raise NotImplementedError

    def parse_host(self, host=None):
        '''Parse host string into user, hostname and port.'''
        if host is None:
            host = self.host
        p = '^((?P<user>[^@]+)@)?(?P<host>[^:]+)(:(?P<port>[0-9]+))?$'
        m = re.search(p, host.strip())
        return {
            'user': m.group('user'),
            'hostname': m.group('host'),
            'port': m.group('port'),
        }

    def path_require(self, path):
        if not files.exists(path):
            abort('directory "{path}" does not exist.'.format(path=path))

    def php_array(self, arr, depth=1):
        """Converts a dictionary to a php array.

             see http://codesnippets.joyent.com/posts/show/252"""
        ret = ""
        lis = []
        if isinstance(arr, type([])):
            for ele in arr:
                if isinstance(ele, (type([]), type({}))):
                    lis.append(self.php_array(ele, depth + 1))
                elif isinstance(ele, (type(1), type(1.0))):
                    lis.append(str(ele))
                else:
                    lis.append("'%s'" % str(ele))
        elif isinstance(arr, type({})):
            for (k, v) in arr.items():
                item = "'" + str(k) + "' => "
                if isinstance(v, (type([]), type({}))):
                    item += (self.php_array(v, depth + 1))
                else:
                    if isinstance(v, (type(1), type(1.0))):
                        item += (str(v))
                    else:
                        item += ("'%s'" % str(v))
                lis.append(item)
        else:
            raise NameError("Neither a array or a dictionary was passed.")
        if len(lis) > 0:
            tab = '  '
            delim = ",\n" + tab * depth
            ret = "array(\n" + tab * depth + delim.join(lis) + "\n"
            ret += tab * (depth - 1) + ")"
        else:
            ret = "array()"
        return ret

    def php_header(self):
        return dedent('''
        <?php /* code generated by dm https://bitbucket.org/brianfisher/dm */
        ''').strip()

    def roledefs_set(self):
        env.roledefs[ROLE_DST] = [self.d().host]
        if self.s() is not None:
            env.roledefs[ROLE_SRC] = [self.s().host]

    def role_key(self, role=ROLE_DST):
        if role == ROLE_SRC:
            return self.c()._dm.src_key if 'src_key' in self.c()._dm else None
        elif role == ROLE_DST:
            return self.c()._dm.dst_key

    def rsync(self, src_path, dst_path, options=RSYNC_OPT_DEFAULT,
              excludes=[]):
        if self.s():
            host = self.parse_host(self.s().host)
        else:
            host = self.parse_host('localhost')

        if host['hostname'] in ['', '127.0.0.1', 'localhost']:
            if '--compress' in options:
                options.remove('--compress')
            if not host['user'] and not host['port']:
                host['hostname'] = ''

        src = ''
        if host['hostname']:
            if host['user']:
                src += host['user'] + '@'
            src += host['hostname'] + ':'
        src += src_path

        dst = dst_path

        excludes = map(lambda e: '--exclude={0}'.format(e), excludes)
        options.extend(excludes)

        self.system().rsync(src, dst, host['port'], options)
        self.system().chgrp(
            paths=dst, group=self.system().group('server'), options='-R')

    def scm_git_clone(self, url, path='', branch=None, tag=None):
        '''Checkout a git repository.'''
        run("git clone {u} {p}".format(p=path, u=url))
        with cd(path):
            if branch:
                with settings(warn_only=True):
                    cmd = "git branch {b} origin/{b}".format(b=branch)
                    run(cmd)
                cmd = "git checkout {b}".format(b=branch)
                run(cmd)
            if tag is not None:
                cmd = "git checkout {t}".format(t=tag)
                run(cmd)

    def scm_git_branch_get(self, path):
        '''Get the current branch.'''
        with cd(path):
            result = run('git branch')
            if result.succeeded:
                for line in result.strip().split('\n'):
                    if line[0:2] == '* ':
                        return line[2:].strip()
        return None

    def status_header(self, header):
        print('')
        print(header)
        print('-'*len(header))
        print('')

    def virtualhost_name(self):
        return 'dm.{0}.{1}'.format(self.c().shortname, self.role_key())

    def yaml_pkg_load(self, filename):
        '''Loads a package yaml file.'''
        yaml = pkg_resources.ResourceManager().resource_filename(
            __name__, filename)
        return AttrDict.from_yaml(yaml)
