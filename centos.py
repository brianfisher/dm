from . import Unix
from fabric.api import settings
from fabric.contrib import files


class Centos(Unix):
    '''
    Dm class for an Ubuntu operating system.
    '''

    def group(self, group_type='server'):
        if group_type == 'server':
            return 'apache'
        return None

    def virtualhost_apache_enable(self, name, template, template_path,
                                  context):
        # assumes following appended to /etc/httpd/conf/httpd.conf
        #   Include /etc/httpd/sites/*
        conf_path = '/etc/httpd/sites/{name}'.format(name=name)
        # write virtualhost conf file
        files.upload_template(
            template_dir=template_path,
            filename=template,
            destination=conf_path,
            context=context,
            use_sudo=self.sudo,
            use_jinja=True,
            backup=False)
        # enable
        self.run("/etc/init.d/httpd reload")

    def virtualhost_apache_disable(self, name):
        with settings(warn_only=True):
            self.rm('/etc/httpd/sites/{name}'.format(name=name))
        self.run("/etc/init.d/httpd reload")
