from . import Unix
from fabric.api import settings
from fabric.contrib import files


class Ubuntu(Unix):
    '''
    Dm class for an Ubuntu operating system.
    '''

    def virtualhost_apache_enable(self, name, template, template_path,
                                  context):

        conf_path = '/etc/apache2/sites-available/{name}'.format(name=name)
        # write virtualhost conf file
        files.upload_template(
            template_dir=template_path,
            filename=template,
            destination=conf_path,
            context=context,
            use_sudo=self.sudo,
            use_jinja=True,
            backup=False)
        # enable
        with settings(warn_only=True):
            self.run("a2ensite {name}".format(name=name))
        self.run("/etc/init.d/apache2 reload")

    def virtualhost_apache_disable(self, name):
        with settings(warn_only=True):
            self.run("a2dissite {name}".format(name=name))
            self.rm('/etc/apache2/sites-available/{name}'.format(name=name))
        self.run("/etc/init.d/apache2 reload")

    def virtualhost_nginx_enable(self, name, template, template_path, context):
        conf_path = '/etc/nginx/sites-available/{name}'.format(name=name)
        # write virtualhost conf file
        files.upload_template(
            template_dir=template_path,
            filename=template,
            destination=conf_path,
            context=context,
            use_sudo=self.sudo,
            use_jinja=True,
            backup=False)
        # enable
        with settings(warn_only=True):
            self.run("ln -s {d} /etc/nginx/sites-enabled".format(d=conf_path))
        self.run("/etc/init.d/nginx restart")

    def virtualhost_nginx_disable(self, name):
        paths = [
            '/etc/nginx/sites-available/{name}'.format(name=name),
            '/etc/nginx/sites-enabled/{name}'.format(name=name),
            ]
        self.run('rm -f {0}'.format(' '.join(paths)))
        self.run("/etc/init.d/nginx restart")
