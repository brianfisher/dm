from fabric.api import abort, env, prompt
import fabric.tasks
import re

# Dm task class


class Task(fabric.tasks.Task):

    name = None
    dependencies = []
    role = 'dst'

    def arg_bool(self, val, default):
        '''
        Attempt to convert task argument value from string to boolean. Prompts
        for value if necessary.'''

        if val == '':
            return default
        if val in (True, False):
            return val
        if re.match(r'(false|no|0)$', val, re.I):
            return False
        if re.match(r'(true|yes|1)$', val, re.I):
            return True
        msg = 'Unexpected argument value "{0}" (use true|yes|1 or false|no|0).'
        msg += ' OK to default to {1}?'
        msg = msg.format(val, default)
        ok = prompt(msg, default='y')
        if ok.lower() != 'y':
            abort("aborting...")
        return default

    def configure(self):
        env.dm.conf().task(self.name)

    def get_hosts(self, arg_hosts, arg_roles, arg_exclude_hosts, env=None):
        """
        Return the host list the given task should be using.

        See :ref:`host-lists` for detailed documentation on how host lists are
        set.
        """
        if self.role is not None and self.role in env.roledefs:
            env.roles = [self.role]
        return super(Task, self).get_hosts(
            arg_hosts, arg_roles, arg_exclude_hosts, env)
