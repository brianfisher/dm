def isstring(string):
    '''Linter marks this as error'''
    return isinstance(string, basestring)
